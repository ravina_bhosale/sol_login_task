﻿CREATE TABLE [dbo].[tblUser] (
    [UserId]    NUMERIC (18) NOT NULL,
    [FirstName] VARCHAR (50) NULL,
    [LastName]  VARCHAR (50) NULL,
    CONSTRAINT [PK_tblUser] PRIMARY KEY CLUSTERED ([UserId] ASC)
);

