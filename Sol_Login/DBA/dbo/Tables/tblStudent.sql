﻿CREATE TABLE [dbo].[tblStudent] (
    [StudentId] NUMERIC (18)  NOT NULL,
    [FirstName] VARCHAR (50)  NULL,
    [LastName]  VARCHAR (50)  NULL,
    [DOB]       DATETIME      NULL,
    [Age]       INT           NULL,
    [Address]   VARCHAR (MAX) NULL,
    [IsDelete]  BIT           NULL,
    CONSTRAINT [PK_tblStudent] PRIMARY KEY CLUSTERED ([StudentId] ASC)
);

