﻿CREATE PROCEDURE [dbo].[uspGetUser]

	@Command Varchar(50)=NULL,

	@UserName Varchar(50)=null,
	@Password Varchar(50)=null,

	@Status int=NULL OUT,
	@Message Varchar(MAX)=NULL OUT

AS

BEGIN

	IF @Command='ValidLogin'

	BEGIN
		
		IF EXISTS(SELECT L.UserName,L.Password 
					FROM tblLogin AS L
						WHERE L.UserName=@UserName AND L.Password=@Password)
						
			BEGIN

				SET @Status=1 
				SET @message='Succesfull Login'

			END
			 
			ELSE

			   BEGIN

				SET @Status=0
				SET @message='UserName and Password are Invalid'

				END
			END

			END
		GO
