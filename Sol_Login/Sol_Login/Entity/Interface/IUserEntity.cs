﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Login.Entity.Interface
{
   public interface IUserEntity
    {
        int UserId { get; set; }

        String UserName { get; set; }

        String Password { get; set; }
    }
}
