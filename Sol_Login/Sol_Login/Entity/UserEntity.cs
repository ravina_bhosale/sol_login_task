﻿using Sol_Login.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Login.Entity
{
    public class UserEntity : IUserEntity
    {
       public int UserId { get; set; }

        public String UserName { get; set; }

        public String Password { get; set; }
    }
}
