﻿using Sol_Login.Entity;
using Sol_Login.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Login.Repository.Interface
{
   public interface IUserRepository
    {
       Task <dynamic> LoginAsync(IUserEntity entityObj);

    }
}
