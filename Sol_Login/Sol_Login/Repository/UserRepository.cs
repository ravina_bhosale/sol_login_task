﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sol_Login.Entity.Interface;
using Sol_Login.ConcreteRepository;
using Sol_Login.Repository.Interface;

namespace Sol_Login.Repository
{
    public class UserRepository : IUserRepository
    {
        #region Declaration
        private UserConcreteRepository userConcreteObj = null;
        #endregion

        #region Constructor
        public UserRepository()
        {
            userConcreteObj = new UserConcreteRepository();
        }
        #endregion

        #region Public Method

        public async Task<dynamic> LoginAsync(IUserEntity entityObj)
        {
            int? status = null;
            String message = null;

            try
            {
                await
                userConcreteObj
                .Get(
                    "ValidLogin",
                    entityObj,
                    (lestatus, lemessage) =>
                    {
                        status = lestatus;
                        message = lemessage;
                    });
                return (status == 1) ? true : false;
            }
            catch(Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
