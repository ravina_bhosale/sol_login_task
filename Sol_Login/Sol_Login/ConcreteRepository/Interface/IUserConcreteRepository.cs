﻿using Sol_Login.CommonRepository.Interface;
using Sol_Login.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Login.ConcreteRepository.Interface
{
   public interface IUserConcreteRepository :IGet<IUserEntity>
    {

    }
}
