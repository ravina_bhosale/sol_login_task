﻿using Sol_Login.ConcreteRepository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sol_Login.Entity.Interface;
using Sol_Login.ORD;

namespace Sol_Login.ConcreteRepository
{
    public class UserConcreteRepository : IUserConcreteRepository
    {
        #region Declaration

        private UserDcDataContext dcObj = null;

        #endregion

        #region constructor

        public UserConcreteRepository()
        {
            dcObj = new UserDcDataContext();
        }

        #endregion

        #region Public method

        public async Task<dynamic> Get(String Command, IUserEntity entityObj, Action<int?, string> storedProcedureOutPara = null)
        {
            int? status = null;
            String message = null;

            try
            {
                return await Task.Run(() =>
                {
                    var getQuery =
                    dcObj
                    .uspGetUser
                    (
                        Command,
                        entityObj.UserName,
                        entityObj.Password,
                        ref status,
                        ref message
                    );

                    storedProcedureOutPara(status, message);

                    return getQuery;

                });
            }

            catch(Exception ex)
            {
                throw;
            }
        }

        #endregion
    }
}
