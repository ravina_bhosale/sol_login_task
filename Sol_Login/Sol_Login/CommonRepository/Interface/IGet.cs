﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Login.CommonRepository.Interface
{
   public interface IGet<TEntity> where TEntity:class 
    {
        Task<dynamic> Get(String Command,TEntity entityObj, Action<int?, String> storedProcedureOutPara = null);
    }
}
